# skrobak/functional_tests.py

# To start with web scraping Scrapy framework must be installed
import os
from scrapy import Spider
from selenium import webdriver
import unittest

class SimpleScrapeTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
    def test_can_start_a_browser_and_run_skrobak(self):
        # Bartek wants to perform first step which is scraping the data from the galeriazyskow.pl
        # In order to do so, he checks the website
        self.browser.get('http://galeriazyskow.pl/')

        # He notices that the website loads smoothly and he observes the page title
        self.assertIn('Galeria Zysków', self.browser.title)

        # The first run of skrobak is made locally; results are being saved to html file
        os.stat("test.html").st_size != 0
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main()
# But is he sure that the content of file contains at least one post from the requested web page?

# Satisfied, Bartek is wondering what would happen if he forced skrobak to scrape once again?
# The gathered posts will be overwritten, doubled or simply disappeared?

# Time for coffee, everything looks fine.



# Next day: is it worth to run scraper again? (Do new posts appeared on the website?)