import scrapy

class SimpleSpider(scrapy.Spider):
    name = 'simple'
    
    def start_requests(self):
        urls = ['http://galeriazyskow.pl/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        filename = 'test.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        # self.log('Saved file %s' & filename)