# skrobak/tests.py

# trivial failing test
from spiders.simple_spider import SimpleSpider
import scrapy
import unittest
import types

class SimpleSpiderTest(unittest.TestCase):

    def test_spider_name(self):
        self.assertIsNotNone(SimpleSpider.name)

    def test_if_start_requests_returns_generator(self):
        self.assertIsInstance(SimpleSpider.start_requests(self), types.GeneratorType)
    
    def test_scraped_page_contains_html(self):
        self.assertIn('<html>', SimpleSpider.parse(self, scrapy.http.Response(url='http://galeriazyskow.pl/')))
    # def test_start_url_returns_correct_html(self):
    #     request = scrapy.Request()
    #     response = SimpleSpider.start_requests()
    #     html = response.text
    #     self.assertTrue

if __name__ == '__main__':
    unittest.main()